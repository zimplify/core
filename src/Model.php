<?php
    namespace Zimplify\Core;
    use Zimplify\Common\ArrayUtils as arrays;
    use Zimplify\Common\DataUtils as datas;
    use Zimplify\Common\EncodeUtils as encoders;
    use Zimplify\Common\ObjectUtils as objects;
    use Zimplify\Common\StringUtils as strings;
    use Zimplify\Common\XmlUtils as xmls;
    use Zimplify\Core\Application as app;
    use Zimplify\Core\EventCycle as cycle;
    use Zimplify\Core\Request as request;
    use \SimpleXMLElement as xml;

    /**
     * the Model class acts as the fundamental base for all features available to any model instances within the system. data objects such
     * as document and other items all will implement some form of model to create objects.
     * 
     * @package Zimplify (10)
     * @subpackage Core (01)
     * @category Instance (01)
     * @api: Model (02)
     */
    class Model {

        const CFG_CLASS_DEBUG = "application.debug";
        const CFG_DATASOURCES = "datasource";
        const CFG_DS_ENGINE = "engine";
        const CFG_DS_SETTINGS = "settings";
        const CFG_ENTROPY = "application.parameters.id.length";
        const CFG_KEY_DATA = "application.paremeters.vault.data";
        const CMD_PROP_SOURCE = "source";
        const DEF_DS_DEFAULT = "default";        
        const DEF_PRO_OBJECT = "object";
        const ERR_NOT_FOUND = 10010102001;
        const ERR_UNKNOWN_ENGINE = 10010102002;
        const ERR_FAIL_SAVE = 100010102003;
        const ERR_NOT_DEFINED = 100010102004;
        const FLD_DEFAULT= "default";
        const FLD_ENCRYPT= "encrypt";
        const FLD_FACTOR= "factor";
        const FLD_TYPE = "type";
        const FLD_OWNER_TYPE = "owner.type";
        const FLD_OWNER_ID = "owner.id";
        const FLD_CREATED = "created";
        const PFX_FEATURES = "FEAT_";
        const XFIELD_REQUIRED = "required";
        const XPATH_GET_COMMAND = "//command[@name='<c>']";
        const XPATH_GET_PROPERTY = "//metadata/";
        const XPATH_GET_REQUIRED = "//metadata//data[@required]";

        protected $id = null;
        private $instance = null;
        private $metadata = null;
        private $owner = null;
        private $unique = true;
        private $events = null;

        /**
         * creating the initial instance for default event mounts
         * @return void
         */
        function __construct() {
            $this->events = new cycle();
        }

        /**
         * cloning the instance 
         * @return void
         */
        function __clone() {
            $this->id = null;
            $this->unique = true;
            $this->initialize($this->owner, null, [], $this->describe());
        }

        /**
         * our magic getter to make sure we can secure the data both at object level and at PHP level
         * @param string $name the field to load out
         * @return mixed the value to load out
         */
        function __get(string $name) {
            if (property_exists($this, $name)) {
                return $this->{$name};
            } else {
                return $this->locate($name);
            }
        }

        /**
         * check if property exists or not
         * @param 
         * @return bool true if exists either as instance property or class properties
         */
        function __isset(string $name) {
            if (property_exists($this, $name)) {
                return $this->{$name};
            } else {
                return $this->locate($name);
            }
        }        

        /**
         * our magic setter to make the setting data become seemless
         * @param string $name the name of the parameter to set
         * @param mixed $value the value to add to the property
         * @return void
         */
        function __set(string $name, $value) {            
            if (property_exists($this, $name)) 
                $this->{$name} = $value;
            else
                $this->define($name, $value);
        }

        public function __dump() {
            return $this->instance;
        }

        /**
         * loading up the root Model object available.
         * @return Model object
         */
        protected function ancestor() : Model {
            return $this->owner ?? $this->owner->parent();
        }

        /**
         * defining the instance property stored in instance data
         * @param string $field the field to set the data with
         * @param mixed $value the value to defined
         */
        public function define(string $field, $value, array $inputs = []) : self {
            $e = false;
            $t = null;
            if ($this->validate($field, $t, $e)) {
                $v = encoders::unformat($t, $value, $inputs);   
                if ($e && !is_null($v)) $v = app::plug("crypto", ["key" => app::env(self::CFG_KEY_DATA)])->encrypt($v);                
                $this->instance = json_encode(arrays::intract(($this->instance ? json_decode($this->instance, true) : []), $field, $v));
                return $this;
            } else {
                $c = explode(app::DLT_DOUBLE_SLASH, get_class($this));
                error_log(end($c)."> undefined property detected - $field.");
            }            
        }

        /**
         * return the metadata of this instance
         * @return \SimpleXMLElement the instance metaadata
         */
        public function describe() : xml {
            return $this->metadata;
        }

        /**
         * remove the instance data
         * @return bool the result from deletion
         */
        public function delete(array $inputs = []) : bool {
            // PHASE 0 - complete predelete
            $this->events->emit("predelete");
            
            // PHASE 1 - loading the command
            if (count($c = $this->describe()->xpath(str_replace("<c>", __FUNCTION__, self::XPATH_GET_COMMAND))) > 0) {

                // PHASE 2 - find the datasource
                if (array_key_exists(self::CMD_PROP_SOURCE, ($s = $c[0]->attributes()))) {
                    $s = (string)$s[self::CMD_PROP_SOURCE];                    
                } else                     
                    $s = self::DEF_DS_DEFAULT;

                // PHASE 3 - loading out the data
                $r = $this->envault($s)->run((new request($c[0]))->export());

                // PHASE 4 - return the result 
                if ($r) {
                    $this->events->emit("deleted", $inputs);
                    return $this;
                } else 
                    throw new \RuntimeException("Unable to save instance", self::ERR_FAIL_SAVE);
            } else 
                throw new \RuntimeException("Command ".__FUNCTION__." undefined.", self::ERR_NOT_FOUND);            
        }

        /**
         * connect the instance with its data plugin
         * @param string $datasource the name for the source of data
         * @return object the data plugin
         */
        private function envault(string $datasource) {

            // PHASE 0 - load datasource details
            if (!$d = app::env(self::CFG_DATASOURCES.".".$datasource))                 
                throw new \UnexpectedValueException("Unable to locate datasource $s.", self::ERR_NOT_FOUND);

            // PHASE 1 - load the adaptor
            if (!array_key_exists(self::CFG_DS_ENGINE, $d))  
                throw new \InvalidArgumentException("No engine specified in datasource $s.", self::ERR_UNKNOWN_ENGINE);

            // PHASE 2 - sending back the plugin
            return app::plug($d[self::CFG_DS_ENGINE], $d);

        }

        /**
         * initializing the instance with data import and organization
         * @param Model (optional) $parent the parent linked object that can form some kind of hierrachy tree of ownership
         * @param string $id (optional) the ID of the instance. If not provided, will assume is a new instance thus will create a new ID
         * @param array $preload (optional) data that will be presetted into the instance once load. only work if the ID is defined. 
         * @param \SimpleXMLElement $meta (optional) the metadata structure of the instance without us accessing the cache.
         */
        public function initialize(self $parent = null, string $id = null, array $preload = [], xml $meta = null) {
            // Phase 0 - get hold of the metadata
            $this->metadata = $this->stage($meta);
            $this->type = objects::classify($this);
            $this->events->emit("created");

            // Phase 1 - loading instance data
            if ($id) {
                $this->id = $id;
                if ($this->populate(count($data) > 0 ? $data : $this->load())) {
                    $this->unique = false;
                    $this->events->emit("mounted");
                } else 
                    throw new \RuntimeException("Instance $id does not exists.", self::ERR_NOT_FOUND);                
            }

            // Phase 2 - loading initial data if not defined
            if ($this->unique) {
                $this->owner = $parent;
                $this->id = $this->provision();                
                $this->events->emit("mounted");                
            }
        }

        /**
         * check if a property query is factored, if so we adjust the results... 
         * @param string $query the query to our wanted parameter
         * @return string the updated query
         */
        private function isFactored(string $query) : string {
            // these are our dummy variables
            $t = $d = $f = null; 
            $e = false;

            // these are need for alteration if required
            $i = -1;

            // this is for query and validation
            $q = "";
            
            // now checking through the tree and see if we get a factored element
            foreach (explode(app::DLT_DOT, $query) as $c) 
                if ($this->validate(($q .= $c), $t, $e, $d, $f))  {
                    $i++;
                    if (!is_null($f)) 
                        break;
                }

            // now we need to change the factor in if required
            if (!is_null($f) && $i > -1) {
                $r = explode(app::DLT_DOT, $query);
                array_splice($r, $i + 1, 0, $f);
            } else 
                $r = $query;
            
            return $r;
        }

        /**
         * make sure the metadata compliance is enforced
         * @param \SimpleXMLElement $source (optional) the source to compare when checking compliance
         * @return bool whether is compliant for persistance
         */
        public function isReady(xml $source = null) : bool {
            $r = true;
            $s = $source ?? $this->describe();
            $n = []; array_push($n, $s->getName());
            
            // loop through the whole metadata
            foreach ($this->metadata->xpath(self::XPATH_GET_REQUIRED) as $n) {
                $a = xmls::normalize($n);
                $d = array_key_exists(self::DEF_DS_DEFAULT, $a) ? $a[self::DEF_DS_DEFAULT] : null;
                $q = array_key_exists(self::XFIELD_REQUIRED, $a) ? $a[self::XFIELD_REQUIRED] : null;
                if ($q === "true") {
                    $v = datas::translate(xmls::path($n));
                    $o = $this->{$v};
                    if (is_null($this->{$v})) {
                        if (!is_null($d)) 
                            $this->{$v} = datas::evaluate($d, $this);
                    }
                }
            }
            return $r;
        }


        /**
         * loading the data from for the instance to use
         * @return array the data to return
         */
        protected function load() : array {

            // PHASE 0 - find the command
            if (count($c = $this->describe()->xpath(str_replace("<c>", __FUNCTION__, self::XPATH_GET_COMMAND))) > 0) {

                // PHASE 1 - find the datasource
                if (array_key_exists(self::CMD_PROP_SOURCE, ($s = $c[0]->attributes()))) {
                    $s = (string)$s[self::CMD_PROP_SOURCE];                    
                } else                     
                    $s = self::DEF_DS_DEFAULT;

                // PHASE 2 - loading out the data
                $r = $this->envault($s)->run((new request($c[0], $this))->export(), $this);

                // PHASE 3 - returning the result
                return json_decode($r, true);
            } else 
                throw new \RuntimeException("Command ".__FUNCTION__." undefined.", self::ERR_NOT_FOUND);
        }

        /**
         * loading the data from stored instance data
         * @param string $field the field to lookup and get
         * @param array $inputs (optional) supplemental data for getter.
         * @return mixed the value that is stored
         */
        private function locate(string $field, array $inputs = []) { 
            $t = $d = null;
            $e = false;
            if ($this->validate($field, $t, $e, $d)) {                
                $v = arrays::extract(json_decode($this->instance, true), $this->isFactored($field));
                if (!is_null($v) && $e)
                    $v = app::plug("crypto", ["key" => self::CFG_KEY_DATA])->decrypt($v);     
                $v = $v ?? $d;            
                return !is_null($v) ? encoders::format($t, $v, $inputs) : null;
            } else 
                throw new \InvalidArgumentException("Property $field is not valid.", self::ERR_NOT_DEFINED);
        }

        /**
         * get the owner of this object
         * @return mixed the owner value
         */
        public function parent() {
            return ($this->owner = $this->owner ?? app::load($this->{self::FLD_OWNER_TYPE}, $this->{self::FLD_OWNER_ID}));
        }

        /**
         * generate a random UUID to ensure object uniqueness
         * @return string the new UUID
         */
        private function provision() : string {
            $r = [];
            for ($x = 0; $x < app::env(self::CFG_ENTROPY); $x++) array_push($r,  uniqid(""));
            return implode(app::DLT_DASH, $r);
        }

        /**
         * force update a data object with $data 
         * @param array $data the input data to write.
         * @param bool $force if data already exists, do we force overwrite? True to overwrite.
         * @return Model the instance after update
         */
        public function populate(array $data, bool $force = false) {
            foreach ($data as $f => $v) {
                if (count($s = explode(app::DLT_DOUBLE_COLON, $f)) > 1) 
                    if (is_null($c = $this->factor($s[0])->{$s[1]}) || $force) 
                        $this->{$s[1]} = $v;
                else 
                    $this->{$s[0]} = $v; 
            }
            return count($data) > 0 ? $this : null;
        }

        /**
         * the checks and automation we need to do before we issue the persistance command
         * @param array $inputs (optional) the special parameters we might need before saving
         * @return void
         */
        protected function preflight(array $inputs = []) : void {
            if ($this->owner) {
                $this->{self::FLD_OWNER_TYPE} = objects::classify($this->parent());
                $this->{self::FLD_OWNER_ID} = $this->parent()->id;
            }
            $this->{self::FLD_CREATED} = datas::evaluate("now", $this);
            $this->{self::FLD_TYPE} = objects::classify($this);      
        }

        /**
         * saving the data into persistence.
         * @param array $inputs (optional) the additional data that helps with processing but is not available in the instance
         * @return Model the instance after saving
         */
        public function save(array $inputs = []) : self {

            // PHASE 0 - if the instance is first instance, make sure we run the preflight
            if ($this->unique) {
                $this->events->emit("presave", $inputs);
                $this->preflight($inputs);
            }

            // PHASE 1 - the check if the instance have required data
            if ($this->isReady()) {
                if (count($c = $this->describe()->xpath(str_replace("<c>", __FUNCTION__, self::XPATH_GET_COMMAND))) > 0) {

                    // PHASE 2 - find the datasource
                    if (array_key_exists(self::CMD_PROP_SOURCE, ($s = $c[0]->attributes()))) {
                        $s = (string)$s[self::CMD_PROP_SOURCE];                    
                    } else                     
                        $s = self::DEF_DS_DEFAULT;

                    // PHASE 3 - loading out the data
                    $r = $this->envault($s)->run((new request($c[0], $this))->export(), $this);

                    // PHASE 4 - return the result 
                    if ($r) {
                        $this->events->emit("saved", $inputs);
                        return $this;
                    } else 
                        throw new \RuntimeException("Unable to save instance", self::ERR_FAIL_SAVE);
                } else 
                    throw new \RuntimeException("Command ".__FUNCTION__." undefined.", self::ERR_NOT_FOUND);
            } else 
                throw new \RuntimeException("The instance does not have the required data to persist.", self::ERR_FAIL_SAVE);
        } 

        /**
         * loading the metadata into the instance
         * @return \SimpleXMLElement the resulted metadata structure
         */
        private function stage(xml $source = null) : xml {
            if (is_null($source)) {
                $c = explode(app::DLT_DOUBLE_SLASH, get_class($this));
                $t = app::read("model", strings::uncamel(end($c), app::DLT_DOT));
            } else 
                $t = $source;            

            // loading the full structure        
            $r = (app::plug("object-loader"))->parse(simplexml_load_string($t));

            // now loading all the constants
            foreach (objects::constants($this) as $n => $v) {                
                if (strpos($n, self::PFX_FEATURES) !== false) {
                    $r = xmls::extend($r, simplexml_load_string($v), self::XPATH_GET_PROPERTY);
                }

            }

            // now return the result
            return $r;
        }
        
        /**
         * validitng a property is accepted in the instance
         * @param string $field the field to load up
         * @param string $type (ref) the type of parameter to read out
         * @param bool $encrypt (ref) whether the field is encrypted
         * @param string $default (ref) whether the default value is defined
         * @param string $factor (ref) whether the node uses factor.
         * @return bool the result of the validation. TRUE if available.
         */
        private function validate(string $field, string &$type = null, bool &$encrypt = false, string &$default = null, 
            string &$factor = null) : bool {

            // PHASE 0 - define our query
            $q = []; foreach (explode(app::DLT_DOT, $field) as $i) array_push($q, "data[@name='$i']");
            $n = null;
            
            // PHASE 1 - capture the node
            if (count($d = $this->describe()->xpath(self::XPATH_GET_PROPERTY.implode(app::DLT_SINGLE_SLASH, $q))) > 0) {
                $n = $d[0];
            } 
            
            // PHASE 2 - capture encrypt and other things...
            if (!is_null($n)) {
                $a = xmls::normalize($n);
                $type = array_key_exists(self::FLD_TYPE, $a) ? (string)$a[self::FLD_TYPE] : self::DEF_PRO_OBJECT;
                $encrypt = array_key_exists(self::FLD_ENCRYPT, $a) ? ((string)$a[self::FLD_ENCRYPT] === "true" ? true : false) : false;
                $default = array_key_exists(self::FLD_DEFAULT, $a) ? (string)$a[self::FLD_DEFAULT] : null;
                $factor = array_key_exists(self::FLD_FACTOR, $a) ? (string)$a[self::FLD_FACTOR] : null;
                return true;
            } else 
                return false;
        }
    }