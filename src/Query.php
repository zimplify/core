<?php
    namespace Zimplify\Core;
    use Zimplify\Common\ArrayUtils;
    use Zimplify\Core\Application;
    use Zimplify\Core\Model;
    use Zimplify\Core\Request;
    
    /**
     * the Query is the object searcher for Models within the applications
     * 
     * @package Zimplify (10)
     * @subpackage Core (01)
     * @category Instance (01)
     * @api: Query (08)
     */
    final class Query {
        
        const CFG_DATASOURCES = "datasource";
        const CLS_DOCUCMENT = "Zimplify\\Core\\Dociment";
        const CFG_DS_ENGINE = "engine";
        const ERR_UNKNOWN_ENGINE = 10010108001;
        const SQL_DATA = "data";

        /**
         * connect the instance with its data plugin
         * @param string $datasource the name for the source of data
         * @return object the data plugin
         */
        private static function engage() {

            // PHASE 0 - load datasource details
            if (!$d = app::env(self::CFG_DATASOURCES.".default"))                 
                throw new \UnexpectedValueException("Unable to locate datasource $s.", Model::ERR_NOT_FOUND);

            // PHASE 1 - load the adaptor
            if (!array_key_exists(self::CFG_DS_ENGINE, $d))  
                throw new \InvalidArgumentException("No engine specified in datasource $s.", self::ERR_UNKNOWN_ENGINE);

            // PHASE 2 - sending back the plugin
            return app::plug($d[self::CFG_DS_ENGINE], $d);

        }

        /**
         * checking the type to be a document or model
         * @param string $type the type to validate
         * @return bool true if is a document. otherwise will be a model
         */
        private static function filter(string $type) : bool {
            $t = explode(Application::DLT_DOUBLE_COLON, $type);
            $r = Application::create((count($t) > 1 ? $t[1] : $t[0]), null, (count($t) > 1 ? $t[0] : null));
            return is_subclass_of($r, self::CLS_DOCUCMENT);
        }

        /**
         * generate a Data Query Request for NoSQL or T-SQL or GraphQL
         * @param string $type the instance type to look for
         * @param array $criteria (optional) the filters to search for
         * @param bool $document (optional) the type of instance. default to regular model
         * @return Request the data request
         */
        private static function prepare(string $type, array $criteria = [], bool $document = false) : Request {

        }

        /**
         * search and identify models and documents in the system
         * @param string $type the instance to look for
         * @param array $criteria the list of filter we can apply
         * @return array the result dataset in instance Document or Model
         */
        public static function search(string $type, array $criteria = []) : array { 
            
            // assume the request
            $q = static::prepare($type, $criteria, static::filter($type));

            // now we are running the query
            $c = static::engage()->run($q);

            // dealing with the result
            $r = [];
            foreach ($c as $i) 
                if (array_key_exists(self::SQL_DATA, $i)) {
                    $d = json_decode($i[self::SQL_DATA], true);
                    $t = ArrayUtil::extract($d, model::FLD_TYPE);
                    array_push($r, Application::create($t, null, $d));
                }
            
            // return the data
            return $r;
        }
    }