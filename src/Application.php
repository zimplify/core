<?php
    namespace Zimplify\Core;
    use Zimplify\Common\ArrayUtils as arrays;
    use Zimplify\Common\CacheUtils as caches;
    use Zimplify\Common\ObjectUtils as objects;
    use Zimplify\Common\StringUtils as strings;
    use Zimplify\Core\Model as model;
    use Zimplify\Core\Query as query;
    use \Exception as error;

    /**
     * the Application is a core instance and have tons of helper functions that we need along the way when using Model, Controller or Views.
     * 
     * @package Zimplify (10)
     * @subpackage Core (01)
     * @category Instance (01)
     * @api: Application (01)
     */
    abstract class Application {

        const CACHE_ENV = "json.settings";
        const DEF_PLUGINS = "plugins";
        const DEF_ZIMPLIFY_CORE = "zimplify.core";
        const DLT_DASH = "-";
        const DLT_DOT = ".";
        const DLT_DOUBLE_SLASH = "\\";
        const DLT_DOUBLE_COLON = "::";
        const DLT_SINGLE_SLASH = "/";
        const ERR_NOT_FOUND = 10010101001;
        const ERR_CLASS_UNKNOWN = 10010101002;
        const ENV_APP_NAME = "application.name";
        const FILE_SETTINGS = "zimplify.json";
        const PATH_OPTIMIZED = "./assets/optimize";
        
        /**
         * generate a new application object that we will be using within the application.
         * @param String $type the type of object we want to create
         * @param Object $owner the parent instance for the resulted object. 
         * @param Array $data the 
         */
        public static function create(string $type, model $owner = null, array $data = []) : model {                    
            if (class_exists($c = objects::unclassify(static::name(), $type))) {
                $r = new $c($owner);
                $r->initialize($owner);
                if (count($data) > 0) 
                    $r->populate($data);                                    
                return $r;
            } else
                throw new \UnexpectedValueException("Object $c does not exists.", self::ERR_NOT_FOUND);            
        }

        /**
         * get settings from environment setup based on $setting parameter.
         * @param string $setting and dotted seperated string with the parameter path
         * @param string $module (optional) submodule we need to read from
         * @return mixed the setting if exists, null if not found.
         */
        public static function env(string $setting, string $module = null) {
            if (!($d = caches::get(self::CACHE_ENV)))
                if (file_exists(self::FILE_SETTINGS)) 
                    $d = file_get_contents(self::FILE_SETTINGS);
                else 
                    throw new \RuntimeException("No configuration setting file.", self::ERR_NOT_FOUND);
            return arrays::extract(json_decode($d, true), ($module ? "application.modules.$module.$setting" : $setting));
                
        }

        /**
         * loading up an existing instance of $type based on the $id provided.
         * @param String $type the type of instance to load
         * @param String $id  the UUID for the instance
         * @param Array (optional) $data the populatable array.
         * @param Boolean (optional) $halt should application halt if the instance is not found
         * @return Model the instance after loading
         */
        public static function load(string $type, string $id, array $data = [], bool $halt = true) {
            try {
                if (class_exists($c = objects::unclassify(static::name(), $type))) {                    
                    $r = new $c();
                    $r->initialize(null, $id, $data);
                    return $r;                 
                } else 
                    throw new \RuntimeException("Reqested object $type is not available.", self::ERR_NOT_FOUND);
            } catch (error $ex) {
                if ($halt) throw $ex;
                return null;
            }
        }

        /**
         * allow the application to look for models
         * @param string $type the type of data to look for
         * @param array $criteria (optional) the filters to apply
         * @return array the result set in models.
         */
        public static function lookup(string $type, array $criteria = []) : array {
            return query::search($type, $criteria);
        }

        /**
         * return the application name stored in the configuration
         * @return String the name of the application
         */
        public static function name() : string {    
            return static::env(self::ENV_APP_NAME);
        }

        /**
         * return the plugin needed for execution
         * @param string $service the name of the service to access.
         * @return Object the plugin instance
         */
        public static function plug(string $service, array $settings = [], bool $halt = true) {
            try {
                if ($c = static::env(self::DEF_PLUGINS.".".$service)) {
                    if (class_exists($c)) {
                        return new $c($settings);
                    } else 
                        throw new \UnexpectedValueException("Plugin for registered service [$c] is missing.", self::ERR_CLASS_UNKNOWN);
                } else 
                    if ($halt)
                        throw new \RuntimeException("Unable to find plugin for $sevice", self::ERR_NOT_FOUND);
            } catch (error $ex) {
                if ($halt) throw $ex;
                return null;
            }
        }

        /**
         * loading pre-cached source code and for the user and determine whether we need to halt the system in case cached item is not found.
         * @param String $type the module type (e.g. model, extension, etc.)
         * @param String $instance the instance we are looking at.
         * @param Boolean $application whether it is an application or core instance we are reading
         * @param Boolean $halt should the application kill itself if the cached item is not available
         * @return String the cached data in ASCII format returned.
         */
        public static function read(string $type, string $instance, bool $application = true, bool $halt = true) : string {
            $i = [($a = $application ? static::name() : self::DEF_ZIMPLIFY_CORE), $type, $instance];
            $a1 = $application ? $a : str_replace(".core", "", self::DEF_ZIMPLIFY_CORE);    
            $c = $application ? "app" : "core";
            $z = explode(self::DLT_DOT, $instance);
            $p = count($z) > 1 ? $z[0] : null;
            $o = end($z);
            $f = self::PATH_OPTIMIZED."/".$type."s/".$c;
            $x = $type;
            
            // loading from cache
            if (!($d = caches::get(implode(".", $i)))) {

                // determine abnormal items
                switch ($type) {
                    case "model": 
                    case "extension": break;
                    case "logic": 
                        $a1 = $application ? $a : str_replace(".core", "", self::DEF_ZIMPLIFY_CORE);
                        $x = "json"; 
                        break;
                    default: $f = "raw";                        
                }

                // build the package path                
                if (file_exists($t = $f."/".($p ? $p."/" : "")."$a1.$o.$x")) 
                    $d = file_get_contents($t);
            }

            if (is_null($d) && $halt) {
                throw new \UnexpectedValueException("The requested instance ".implode(".", $i)." is not available", self::ERR_NOT_FOUND);
            }

            return $d;
        }
    }