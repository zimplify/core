<?php
    namespace Zimplify\Core;
    use Zimplify\Common\XmlUtils;
    use Zimplify\Core\Application;
    use Zimplify\Core\Model;

    /**
     * the View works as a way to forcing out appropriate data available for the user
     * 
     * @package Zimplify (10)
     * @subpackage Core (01)
     * @category Instance (01)
     * @api: View (09)
     */
    class View {

        const ATTR_EXCLUDE = "exclude";
        const DEF_DATA_NAME = "@name";
        const DEF_DATA_PREF = "data[@name='";
        const DEF_DATA_SUFF = "']'";
        const XPATH_GET_METADATA = "//metadata";

        private $owner = null;

        /**
         * creating a new instance
         * @param Model $instance the instance retrieve the view
         * @return void
         */
        function __construct(Model $instance) {
            $this->owner = $instance;
        }
        
        /**
         * destructor for cleaning up
         * @return void
         */
        function __destruct() {
            $this->owner = null;
        }

        /**
         * encoding the instance data and to do checks over exclusions
         * @param SimpleXMLElement the node to analyze
         * @param Model $caller (optional) the instance calling the analysis
         * @return array the result data array
         */
        protected function decide(xml $node, Model $caller = null) {
            $r = null;
            $a = XmlUtils::normalize($node);

            // filter out exclusion, if so return nothing.
            if (array_key_exists(self::ATTR_EXCLUDE, $a)) 
                if ($a[self::ATTR_EXCLUDE] === "true") 
                    return $r;

            // if not excluded then let's get to work
            if (count($n->children()) > 0) {
                $r = [];
                foreach ($n->children() as $n) 
                    $r[$n->getName()] = $this->decide($n, $caller);
            } else 
                return $this->{$this->encode($n)};
            
            return $r;
        }

        /**
         * capture the path of the data in the instance
         * @param SimpleXMLElement $node the node to analyze
         * @return string the path to data
         */
        protected function encode(xml $node) : string {
            $r = [];
            foreach (explode(Application::DLT_SINGLE_SLASH, XmlsUtils::path($node)) as $t)
                if (substr_count($t, self::DEF_DATA_NAME)) 
                    array_push($r, str_replace(self::DEF_DATA_PREF, "", str_replace(self::DEF_DATA_SUFF, "", $t)));
            return implode(Application::DLT_DOT, $r);
        }

        /**
         * transforming instance data to exportable array with specific information
         * @param Model $caller (optional) the caller instance for getting the new.
         * @return array the data in array for export
         */
        public function get(Model $caller = null) : array {
            $r = [];
            foreach ($this->owner->describe()->xpath(self::XPATH_GET_METADATA)[0]->children() as $n) 
                if (($a = $this->decide($n, $caller)) > 0) 
                    $r[$n->getName()] = $a;
            return $r;
        }

    }