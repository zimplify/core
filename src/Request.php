<?php
    namespace Zimplify\Core;
    use Zimplify\Core\Model as model;
    use Zimplify\Common\DataUtils as datas;
    use Zimplify\Common\XmlUtils as xmls;
    use \SimpleXMLElement as xml;

    /**
     * the Request class act as a serializable class that interface can use to structure request that ultimately use for communication with
     * external service.
     * 
     * NOTE: Request have specific structure and needs to be followed, and need to include this in the model source
     * <command name="<name>" source="<provider>" method="<method>">
     *      <attribute type>
     *          <attribute item>value</attribute item>
     *      </attribute type>
     * </command>
     * 
     * Application: Zimplify (10)
     * Bundle: Core (01)
     * Type: Instance (01)
     * Class: Request (04)
     */    
    class Request {

        const ERR_BAD_TYPE = 10010104001;
        const REQ_TYPE_NAME = "command";
        const REQ_ATTR_CALL = "call";
        const REQ_ATTR_SOURCE = "source";
        const REQ_ATTR_METHOD = "method";
        const REQ_ATTR_NAME = "name";
        const TAG_FIELD = "field";
        const TAG_FILTER = "filter";
        const TAG_CONDITION = "condition";
        
        private $provider = null;
        private $command = null;
        private $name = null;
        private $method = null;
        private $attributes = [];

        /**
         * creating a new request instance by filtering out elements that we may need to consider.
         * @param \SimpleXMLElement $request the source request
         */
        function __construct(xml $request = null, model $caller = null) {
            if ($request) 
                $this->stage($request, $caller);
        }

        /**
         * dissect a request and populate the data inside
         * @param \SimpleXMLElement $node the node to dissect into Array
         * @param Model $source the datasource for data
         * @return array an associative array with attribute data
         */
        protected function disseminate(xml $node, model $source) : array {
            $r = [];                
            if (count($node->children()) == 0) {
                $r["value"] = datas::evaluate((string)$node, $source);
                foreach (xmls::normalize($node) as $f => $v)                  
                    $r[$f] = datas::evaluate((string)$v, $source);
            } else {
                $r = [];
                foreach ($node->children() as $n) {
                    switch ($n->getName()) {
                        case self::TAG_CONDITION:
                        case self::TAG_FIELD:
                        case self::TAG_FILTER:
                            array_push($r, $this->disseminate($n, $source));
                            break;
                        default:
                            $r[$n->getName()] = $this->disseminate($n, $source);
                    }
                }                    
            }
            return $r;
        }

        /**
         * exporting the structure into an array that can be digested by a provider
         * @return array the result data
         */
        public function export() : array {
            return ["command" => $this->command, "method" => $this->method, "attributes" => $this->attributes];
        }

        /**
         * staging the data based on the XML request given
         * @param \SimpleXMLElement $request the request to be staged into the object
         * @return void
         */
        private function stage(xml $request, model $caller = null) {
            if ($request->getName() === self::REQ_TYPE_NAME) {
                $a = $request->attributes();

                // now taking care of the command attribute
                foreach ($a as $f => $v) {
                    switch ($f) {
                        case self::REQ_ATTR_CALL: $this->command = (string)$v; break;
                        case self::REQ_ATTR_NAME: $this->name = (string)$v; break;
                        case self::REQ_ATTR_METHOD: $this->method = (string)$v; break;
                        case self::REQ_ATTR_SOURCE: $this->provider = (string)$v; break;                        
                    }
                }

                // now tracing the elements inside the item
                foreach ($request->children() as $n) 
                    $this->attributes[$n->getName()] = $this->disseminate($n, $caller);                    
                    
            } else
                throw new \UnexpectedValueException("Expected request to be of type <command> but is not.", self::ERR_BAD_TYPE);
        }

    }