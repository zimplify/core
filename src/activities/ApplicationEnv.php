<?php
    namespace Zimplify\Core\Activities;
    use Zimplify\Core\Application;

    /**
     * this task enable reading application configuration data from the environment
     * 
     * @package Zimplify (10)
     * @subpackage Core (01)
     * @category Activity (03)
     * @api: ApplicationEnv (01)
     */
    final class ApplicationEnv {

        public function run(&$source, array &$inputs = [], string &$signal = null) {

        }
    }