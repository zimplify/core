<?php
    namespace Zimplify\Core;

    /**
     * the EventCycle stores the references for events to run within the instance.
     * 
     * Application: Zimplify (10)
     * Bundle: Core (01)
     * Type: Instance (01)
     * Class: EventCycle (03)
     */
    final class EventCycle {

        private $events = [];

        /**
         * emitting new event function
         * @param string $event the Event instance to call
         * @param array $args (optional) the value to parse to the function
         * @return void
         */
        public function emit(string $event, array $args = []) {
            if (array_key_exists($event, $this->events)) {
                $s = $this->events[$event][0];
                $f = $this->events[$event][1];
                $s->{$f}(...$args);
            }
        }

        /**
         * adding an event hook onto the cycle
         * @param String $name the name of the instance
         * @param Object $source the instance to own the trigger method
         * @param String $call the method to trigger upon calling the event that belong to the source
         * @return void
         */
        public function hook(string $name, object $source, string $call) {
            $this->events[$name] = [$source, $call];
            return true;
        }

        /**
         * remove an event already defined in the cycle
         * @param String $name the name of the instance
         * @return void
         */        
        public function unhook(string $name) {
            if (array_key_exists($name, $this->events)) {
                unset($this->events[$name]);
            }
        }
        
    }