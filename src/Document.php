<?php
    namespace Zimplify\Core;
    use Zimplify\Plugins\Workflow\Interfaces\IWorkflowEnabledInterface as workflow;
    use Zimplify\Common\ObjectUtils as objects;
    use Application as app;
    use Model as model;

    /**
     * the Workflow Service Plugin provide the necessary automation for instances to do over a configurable flow for the system.
     * 
     * @package Zimplify (10)
     * @subpackage Core (01)
     * @category Instance (01)
     * @api: Document (06)
     */          
    abstract class Document extends model implements workflow {

        private $author = null;

        const FLD_AUTHOR_ID = "author.id";
        const FLD_AUTHOR_TYPE = "author.type";
        const PLI_WORKFLOW = "workflow";
        const PLI_ARG_SOURCE = "source";

        /**
         * get the author of the document
         * @param Model $writer (optional) will mark the writer as author if is not defined and presented.
         * @return Model the current author of this document
         */
        public function author(model $writer = null) : model {
            if ($writer && is_null($this->{self::FLD_AUTHOR_ID})) 
                $this->author = $writer;

            if (!$this->author) {
                if (($this->{self::FLD_AUTHOR_TYPE} && $this->{self::FLD_AUTHOR_ID})) 
                    $this->author = app::load($this->{self::FLD_AUTHOR_TYPE}, $this->{self::FLD_AUTHOR_ID});
                else 
                    throw new \RuntimeException("Cannot find author when it is not defined.", self::ERR_NOT_FOUND);
            } else 
                return $this->author;
                return ($this->author = $this->author ?? 
                    ($this->{self::FLD_AUTHOR_TYPE} && $this->{self::FLD_AUTHOR_ID} ? 
                        app::load($this->{self::FLD_AUTHOR_TYPE}, $this->{self::FLD_AUTHOR_ID}) : null));
        }

        /**
         * spawn a new workflow plugin and start the named workflow
         * @param string $name the name of the flow to trigger
         * @param array $inputs (optional) the additional parameter that the workflow may require
         * @return mixed the result from the flow
         */
        public function automate(string $name, array $inputs = []) {
            if (objects::isImplemented($this, "IWorkflowEnabledInterface"))
                return app::plug(self::PLI_WORKFLOW, [self::PLI_ARG_SOURCE => $this])->run($name, $inputs);
            else 
                throw new \UnexpectedValueException("Calling automation when no implement workflow.", self::ERR_NOT_DEFINED);
        }

        /**
         * the checks and automation we need to do before we issue the persistance command
         * @param array $inputs (optional) the special parameters we might need before saving
         * @return void
         */
        protected function preflight(array $inputs = []) : void {
            parent::preflight($inputs);
            if ($this->author()) {
                $this->{self::FLD_AUTHOR_ID} = $this->author()->id;
                $this->{self::FLD_AUTHOR_TYPE} = $this->author()->{self::FLD_TYPE};
            }            
        }        
    }