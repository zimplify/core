<?php
    namespace Zimplify\Core;

    /**
     * the Controller act as the C layer in the MVC framework to provide necessary control over what views to generate and data pass thru
     * 
     * @package Zimplify (10)
     * @subpackage Core (01)
     * @category Instance (01)
     * @api: Controller (07)
     */    
    abstract class Controller {

    }