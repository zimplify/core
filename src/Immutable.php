<?php
    namespace Zimplify\Core;
    use Model as model;

    /**
     * the Immutable instance is a special type of instance to enable us to make sure data is 
     * 
     * Application: Zimplify (10)
     * Bundle: Core (01)
     * Type: Instance (01)
     * Class: Model (05)
     */    
    abstract class Immutable extends model {

        const ERR_NOT_ALLOWED = 10010105001;
        
        /**
         * remove the instance data
         * @return bool the result from deletion
         */
        public function delete(array $inputs = []) : bool {
            throw new \RuntimeException("Deletion is not allow on an immutable object.", self::ERR_NOT_ALLOWED);
        }

        /**
         * saving the data into persistence.
         * @param array $inputs (optional) the additional data that helps with processing but is not available in the instance
         * @return Model the instance after saving
         */
        public function save(array $inputs = []) : self {
            if ($this->isUnqiue()) {
                return parent::save($inputs);
            } else
                throw new \RuntimeException("Updates is not allow on an immutable object.", self::ERR_NOT_ALLOWED);
        }        
    }