<?php
    namespace Zimplify\Core\Plugins;
    use Zimplify\Common\XmlUtils as xmls;
    use Zimplify\Plugins\Xml\XmlParserPlugin as parser;
    use Zimplify\Core\Application as app;
    use \SimpleXMLElement as xml;

    /**
     * the Application is a core instance and have tons of helper functions that we need along the way when using Model, Controller or Views.
     * 
     * @package Zimplify (10)
     * @subpackage Core (01)
     * @category Plugin (02)
     * @api: ObjectParserPlugn (01)
     */
    final class ObjectParserPlugin extends parser {

        const ATTR_INHERIT = "inherit";
        const CFG_PLUGIN_ENVS = "services.object-loader";
        const DEF_OBJECT_TYPE = "model";
        const ERR_BAD_SUBNODE = 10010201001;
        const XFIELD_NAME = "name";
        const XPATH_OBJECT = "/object";

        /**
         * creating the new plugin - need to read directly from the app::env with specific tags
         * @param array $args (optional) the arguments to push fornthe plugin
         */
        function __construct (array $args = []) {
            $args["logic"] = json_decode(app::read("logic", "object", false, true), true);
            parent::__construct($args);
            $this->envs = app::env(self::CFG_PLUGIN_ENVS);
        }

        /**
         * complete the initialization of the plugin 
         * @return void
         */
        protected function initialize() {
        }        

        /**
         * the main parse action function
         * @param SimpleXMLElement $target the target structure we want to parse in
         * @param SimpleXMLElement $source the reference source structure to append onto 
         * @return mixed the result after parsing
         */
        public function parse(xml $target, xml $source = null) {
            $r = null;

            // PHASE 0 - make sure syntax is valid
            if ($this->evaluate($target)) {
                $a = xmls::normalize($target);

                // PHASE 1 - now branching to the target actions
                switch ($target->getName()) {
                    // for data
                    case "data": 
                        $r = $target;
                        break;

                    // this is for passthru only
                    case "command":
                    case "mapping": 
                    case "workflow":
                        $r = $target;
                        break;

                    // if the target is an object
                    case "object": 
                        if (array_key_exists(self::ATTR_INHERIT, $a)) {
                            $i = explode(app::DLT_DOUBLE_COLON, $a[self::ATTR_INHERIT]);
                            $a = !(count($i) > 1);
                            $b = app::read(self::DEF_OBJECT_TYPE, end($i), !(count($i) > 1));                            
                            $p = $this->parse(simplexml_load_string($b));
                            foreach ($target->children() as $n) {      
                                if (count($pn = $p->xpath("/object"."/".$n->getName())) > 0) {
                                    $nt = $this->updateOrInsert($pn[0], $n);
                                    $np = str_replace($pn[0]->asXML(), str_replace('<?xml version="1.0"?>', "", $nt->asXML()), $p->asXML());
                                    $p = simplexml_load_string($np);
                                } else 
                                    $p = xmls::extend($p, $n, self::XPATH_OBJECT);
                            }
                        } else 
                            $p = $target;
                        $r = $p;
                        break;

                    // these are compound nodes that capsulate the real nodes.
                    case "commands":
                    case "mappings":
                    case "metadata":
                    case "workflows":
                        $r = $this->updateOrInsert($source, $target);
                        break;
                }
            }
            return $r;
        }

        /**
         * check the $source node to see if the type of $target node exists, if yes, then parse data inside. Otherwise 
         * @param SimpleXMLElement $source the source element to be evaluated as a base 
         * @param SimpleXMLElement $target the target node to be inserted or updated onto the $source
         * @return SimpleXMLElement the resulted combined the $target and $source
         */
        protected function updateOrInsert(xml $source, xml $target) : xml {
            if (count($t = $source->xpath(xmls::path($target))) > 0) {
                $r = $t[0];
                foreach ($target->children() as $n) {
                    $nn = isset($n->attributes()[self::XFIELD_NAME]) ? $n->attributes()[self::XFIELD_NAME] : null;
                    if (!is_null($nn)) {
                        $q = "/".$n->getName()."[@name='$nn']";
                        $nr = $this->parse($n);
                        if (count($cn = $source->xpath("./".$target->getName()."/data[@name='$nn']")) > 0) {
                            $rr = str_replace($cn[0]->asXML(), $nr->asXML(), $r->asXML());
                            $r = simplexml_load_string($rr);
                        } else
                            $r = xmls::extend($r, $n, ".");                        
                    } else 
                        throw new \UnexpectedValueException("The subnode should have a name reference.", self::ERR_BAD_SUBNODE);
                }                     
            } else 
                $r = xmls::extend($source, $target, ".");
            return $r;
        }

    }
